# place-library

`place-library` is a simple learning project.

It's a Vue3 + Typescript + Pinia.js proejct. It permits to collect places you visited on a simple OSM map using leaflet.

It uses nominatim as geocoding service.

All data are saved into the browser `localstorage`.


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### License

Released under MIT license