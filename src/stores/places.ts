import { defineStore } from 'pinia'
import type { LatLng, LatLngExpression, PointExpression } from 'leaflet'
import { Place, PlaceStatus } from '@/models/Place'

export interface State {
  places: Place[]
  centerMap: PointExpression
  markers: LatLngExpression[]
  editPositionPlace: string
  editPositionMarker: LatLngExpression
}

export const usePlacesStore = defineStore('places', {
  state: () =>
    ({
      places: [] as Place[],
      centerMap: [46.0675, 11.1154],
      markers: [] as LatLng[],
      editPositionPlace: '',
      editPositionMarker: {} as LatLngExpression
    }) as State,
  actions: {
    newPlace(pos: LatLngExpression) {
      this.places.push(
        new Place(new Date().toISOString(), '', PlaceStatus.Create, '', pos as LatLng, [])
      )
    },
    savePlace(place: Place) {
      const idx = this.places.indexOf(place)
      place.status = PlaceStatus.Save
      this.places.splice(idx, 1, place)
      // save in localstorage
      localStorage.setItem(place.id, JSON.stringify(place))
    },
    removePlace(place: Place) {
      const idx = this.places.indexOf(place)
      if (idx > -1) {
        this.places.splice(idx, 1)
      }
      const markerIdx = this.markers.indexOf(place.position)
      if (markerIdx > -1) {
        this.markers.splice(markerIdx, 1)
      }
      localStorage.removeItem(place.id)
    },
    cleanEditPosition() {
      this.editPositionPlace = ''
      this.editPositionMarker = {} as LatLngExpression
    },
    loadAll() {
      for (let i = 0; i < localStorage.length; i++) {
        const k = localStorage.key(i)
        if (k != null) {
          const v = localStorage.getItem(k)
          if (v != null) {
            const place: Place = JSON.parse(v)
            if (place.dates) {
              place.dates = place.dates.map((d) => new Date(d))
            }
            this.places.push(
              new Place(
                place.id,
                place.name,
                place.status,
                place.description,
                place.position,
                place.dates ? place.dates : []
              )
            )
          }
        }
      }
    }
  }
})
