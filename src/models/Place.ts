import type { LatLng } from 'leaflet'

export class Place {
  name: string
  description: string
  status: PlaceStatus
  position: LatLng
  id: string
  dates: Date[]

  constructor(
    id: string,
    name: string,
    status: PlaceStatus = PlaceStatus.Create,
    description: string = '',
    position: LatLng,
    dates: Date[]
  ) {
    this.name = name
    this.status = status
    this.description = description
    this.position = position
    this.id = id
    this.dates = dates
  }
}

export enum PlaceStatus {
  Create,
  Save
}
