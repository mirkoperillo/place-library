const formatDate = (d: Date) => {
  const ops = { year: 'numeric' } as Intl.DateTimeFormatOptions
  ops.month = ops.day = '2-digit'
  return new Intl.DateTimeFormat('it-IT', ops).format(d)
}

export default {
  formatDate
}
