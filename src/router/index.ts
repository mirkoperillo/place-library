import { createRouter, createWebHistory } from 'vue-router'
import  Viewer from '../views/Viewer.vue'
import  Editor from '../views/Editor.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/viewer'
    },
    {
      path: '/editor',
      name: 'editor',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Editor.vue')
      //component: Editor
    },
    {
      path: '/viewer',
      name: 'viewer',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Viewer.vue')
      //component: Viewer
    }
  ]
})

export default router
