import { PlaceStatus, Place } from '@/models/Place'
import { LatLng } from 'leaflet'
import { usePlacesStore } from '@/stores/places'

const exportPlaces = () => {
  let exportCsv: string = 'id, name, description, lat, lng, dates\n'

  for (let i = 0; i < localStorage.length; i++) {
    const k = localStorage.key(i)
    if (k != null) {
      const v = localStorage.getItem(k)
      if (v != null) {
        const { id, name, description, position, dates }: Place = JSON.parse(v)
        const datesAsString = dates ? dates.map((d) => new Date(d).getTime()) : []
        let exportDates = ''
        datesAsString.forEach((d) => (exportDates += `${d}|`))
        exportCsv += `${id}, ${name}, ${description}, ${position.lat}, ${position.lng}, "${exportDates}"\n`
      }
    }
  }
  const url = window.URL.createObjectURL(new Blob([exportCsv]))
  const link = document.createElement('a')
  link.href = url
  const fileName = `places.csv`
  link.setAttribute('download', fileName)
  document.body.appendChild(link)
  link.click()
}

const importPlaces = (importFile: File) => {
  importFile
    .text()
    .then((content) => {
      const rows = content.split('\n')
      const placesStore = usePlacesStore()
      rows.forEach((row) => {
        console.log(`row: ${row}`)
        const elem = row.split(',')
        console.log(`elem: ${elem[0]}`)
        if (row.trim().length !== 0 && elem[0] !== 'id') {
          console.log(`${elem[3]} -- ${elem[4]} -- ${elem[5]}`)
          console.log(elem[5].substring(2, elem[5].length - 2).split('|'))
          const dates = elem[5]
            .substring(2, elem[5].length - 2)
            .split('|')
            .map((d) => new Date(parseInt(d)))
          console.log(dates)
          const place = new Place(
            elem[0],
            elem[1],
            PlaceStatus.Save,
            elem[2],
            new LatLng(parseFloat(elem[3]), parseFloat(elem[4])),
            dates
          )
          placesStore.savePlace(place)
        }
      })
    })
    .catch((err) => console.log(err))
}

export default {
  exportPlaces,
  importPlaces
}
