export const search = async (address: string) => {
  try {
    const res = await fetch(`https://nominatim.openstreetmap.org/search?q=${address}&format=jsonv2`)
    console.log(JSON.stringify(res))
    return await res.json()
  } catch (err) {
    return console.log(err)
  }
}
